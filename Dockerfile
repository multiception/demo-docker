FROM ros:noetic-perception

# Install dependencies
RUN apt update -y \
 && apt upgrade -y \
 && apt install -y \
    python-is-python3 \
    git \
    python3-pip \
    ros-noetic-rqt \
    ros-noetic-rviz

# Retrieve the source code
# Weird authentication shenenigans because some submodules are private
# thus requiring a token, and its a pain to use that recursively
ARG USERNAME
ARG GITLAB_ACCESS_TOKEN
RUN git config --system credential.helper store \
 && echo "https://USERNAME:GITLAB_ACCESS_TOKEN@gitlab.utc.fr" > ~/.git-credentials \
 && git clone --recursive https://$USERNAME:$GITLAB_ACCESS_TOKEN@gitlab.utc.fr/multiception/workspace.git -- /ws

WORKDIR /ws

# Install ROS dependencies (rosdep.yaml are custom files that help resolving dependencies)
RUN find /ws/src -name rosdep.yaml -exec echo "yaml file:{}" >> /etc/ros/rosdep/sources.list.d/50-hds-custom.list \; \
 && rosdep update \
 && rosdep install --from-paths src --ignore-src -y \
 && pip3 install --upgrade numpy scipy # Manually updating those because rosdep is satisfied with old versions

# Build the workspace
RUN . /opt/ros/noetic/setup.sh \
 && catkin build -DCMAKE_BUILD_TYPE=Release

COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT /entrypoint.sh
