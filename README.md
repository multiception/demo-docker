# Demo Multiception Docker

Replays videos from multiception at the push of a button!

Well, after typing `docker compose up` that is.
Oh and after setting the following environment variables:

| Name           | Description                                                                                      | Exemple                    |
|----------------|--------------------------------------------------------------------------------------------------|----------------------------|
| DATASETSFOLDER | Path to where datasets are stored, see [here](https://gitlab.utc.fr/hds_vehint/datasets/configs) | `/home/antoine/datasets`   |
| DATASET        | Name of the particular dataset used                                                              | `Multiception/Overtaking3` |

And don't forget to execute `xhost local:docker` beforehand to allow RVIZ to shine through docker.

But other than that it should work without a hitch :smile: once you have the right data locally, as explained in next section.

## Retrieving datasets

Data used in this demo is not part of the docker image because it is too heavy. Instead it is stored on the Lab's FTP servers.
To retrieve them, do the following (with the previously mentionned environment variables set):

```bash
git clone https://gitlab.utc.fr/hds_vehint/datasets/configs -- $DATASETSFOLDER
cd $DATASETSFOLDER
./cli.py pull $DATASET/'zoe*'/organized/span.bag
./cli.py pull $DATASET/'zoe*'/organized/vehicle.bag
./cli.py pull $DATASET/'zoe*'/organized/vlp32C-raw.bag
```

## Building the image

Normally, the docker image should be prebuild and accessible [here](https://registry.gitlab.utc.fr/multiception/demo-docker).

But shall you want to DIY a multiception image, you'll need the following environment variables:

| Name                | Description                                                                                                                                          | Exemple                      |
|---------------------|------------------------------------------------------------------------------------------------------------------------------------------------------|------------------------------|
| GITLAB_USERNAME     | Gitlab's username used to clone the repository                                                                                                       | `limaanto`                   |
| GITLAB_ACCESS_TOKEN | Access token used to clone the repository. Acquired [here](https://gitlab.utc.fr/-/profile/personal_access_tokens) with at least `read_resositories` | `grgan--yqk4iHj6qTbt1E6gBPx` |

and a simple `docker compose build` should suffice.

Updating the registry is done by doing `docker push registry.gitlab.utc.fr/multiception/demo-docker`.
